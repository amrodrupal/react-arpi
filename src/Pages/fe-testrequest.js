import React from "react";
import axios from "axios";
import {Redirect} from 'react-router-dom';

class TestRequest extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  oauthTokenCall(status) {
    if (status === 401) {
      let refresh = localStorage.getItem('refresh');
      let bodyFormData = new FormData();
      bodyFormData.set('grant_type', 'refresh_token');
      bodyFormData.set('refresh_token', refresh);
      bodyFormData.set('client_id', 'd994d914-749a-47bc-90d9-2e24c32b22f8');
      bodyFormData.set('client_secret', 'secret');
      axios.request(
        {
          method: "post",
          url: "http://fw-backend.local/oauth/token",
          data: bodyFormData,
        }
      ).then((response) => {
        localStorage.setItem('access', response.data.access_token);
        localStorage.setItem('refresh', response.data.refresh_token);
        this.componentDidMount();
      }).catch((error) => {
        console.log(error);
        return error;
      });
    }
    if (status === 403) {
      return <Redirect to={'/not-found'}/>;
    }
  }

  componentDidMount() {

    const config = {
      query: `
{
  nodeById(id: "1") {
    ... on NodeCtBasicPage {
      fieldCtTeaserText
      fieldCtParagraphs {
        entity {
          ... on ParagraphPtText {
            fieldPtText {
              value
            }
          }
        }
      }
    }
  }
}`
    };
    let access = localStorage.getItem('access');
    let header = {
      headers: {
        Authorization: "Bearer " + access,
      }
    };
    axios.post(
      'http://fw-backend.local/graphql',
      config,
      header
    ).then((result) => {
        this.setState({
          isLoaded: true,
          items: result
        });
      },
      (error) => {
        this.oauthTokenCall(error.response.status);
      });
  }

  render() {
    const {error, isLoaded, items} = this.state;
    console.log(this.state);
    if (error && !items) {
      return <div>Error: {error.message}</div>;
    }
    else {
      if (!isLoaded) {
        return <div>Loading...</div>;
      }
      else {
        if (items) {
          return (
            <div>Teaser text: {items.data.data.nodeById.fieldCtTeaserText}
              Paragraph text: {items.data.data.nodeById.fieldCtParagraphs[0].entity.fieldPtText.value}</div>
          );
        }
        else {
          return (
            <div>Something went wrong. Empty items.</div>
          )
        }
      }
    }
  }
}

export default TestRequest;
