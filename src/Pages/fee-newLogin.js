import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Service } from "../Services/login";
import { connect } from 'react-redux';

class NewLogin extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      errors: {},
      isLoading: false
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }



  onSubmit (e) {
    e.preventDefault();

    this.setState({isLoading: true});
    const { username, password } = this.state;

    if (username && password) {
      this.props.login(username, password);
    }
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {

    const { loggingIn } = this.props;
    const { errors, username, password, isLoading} = this.state;

    return (

      <Container className={'login-container'}>
        <Form className={'login-form'} onSubmit={this.onSubmit}>

          <Form.Group controlId={'username'}>
            <Form.Label>Username / E-mail:</Form.Label>
            <Form.Control type={'text'} required value={username} error={errors.username} onChange={this.onChange}/>
          </Form.Group>

          <Form.Group controlId={'password'}>
            <Form.Label>Password:</Form.Label>
            <Form.Control type={'password'} required value={password} error={errors.password} onChange={this.onChange}/>
          </Form.Group>

          <Form.Group>
            <Button variant={'primary'} type={'submit'} disabled={isLoading}>Log in</Button>
          </Form.Group>

        </Form>
      </Container>

    )
  }
}

function mapState(state) {
  const { loggingIn } = state.authentication;
  return { loggingIn };
}

const actionCreators = {
  login: Service.login,
};
const connectedLoginPage = connect(mapState, actionCreators)(NewLogin);

export { connectedLoginPage as NewLogin };