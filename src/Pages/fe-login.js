import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import axios from "axios";

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoaded: false,
      items: [],
      error: null
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  login(username, password) {

    let bodyFormData = new FormData();
    bodyFormData.set('grant_type', 'password');
    bodyFormData.set('client_id', 'd994d914-749a-47bc-90d9-2e24c32b22f8');
    bodyFormData.set('client_secret', 'secret');
    bodyFormData.set('username', username);
    bodyFormData.set('password', password);

    axios.request(
      {
        method: "post",
        url: "http://fw-backend.local/oauth/token",
        data: bodyFormData,
      }
    ).then((response) => {
      console.log(response);
      localStorage.setItem('access', response.data.access_token);
      localStorage.setItem('refresh', response.data.refresh_token);
      this.setState({isLoaded: false});
    }).catch((error) => {
      console.log(error);
    });
  }

  onSubmit(e) {
    e.preventDefault();

    this.setState({isLoaded: true});
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    if (username && password) {
      this.login(username, password);
    }
  }

  render() {
    const {isLoaded} = this.state;

    return (
      <Container className={'login-container'}>
        <Form onSubmit={this.onSubmit} className={'login-form'}>

          <Form.Group controlId={'username'}>
            <Form.Label>Username:</Form.Label>
            <Form.Control type={'text'} required/>
          </Form.Group>

          <Form.Group controlId={'password'}>
            <Form.Label>Password:</Form.Label>
            <Form.Control type={'password'} required/>
          </Form.Group>

          <Form.Group>
            <Button variant={'primary'} type={'submit'}
                    disabled={isLoaded}>Login</Button>
          </Form.Group>

        </Form>
      </Container>
    )
  }
}

export default Login;
