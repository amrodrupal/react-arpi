import React from "react";
import axios from "axios";

class TestPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {

    const qs = require('querystring');
      const config = {
        client_id: 'd994d914-749a-47bc-90d9-2e24c32b22f8',
        client_secret: 'secret',
        grant_type: 'password',
        username: 'admin',
        password: 'admin',
      };
      console.log(config);
      const response = axios.post(
        'http://fw-backend.local/oauth/token',
        qs.stringify(config),
      ).then((result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        });
      console.log('Response:' + response);
  }

  render() {
    const {error, isLoaded, items} = this.state;
    console.log(this.state);
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    else {
      if (!isLoaded) {
        return <div>Loading...</div>;
      }
      else {
        if (items) {
          return (
            <div>Access token is: {items.data.access_token}</div>
          );
        }
        else {
          return (
            <div>Something went wrong. Empty items.</div>
          )
        }
      }
    }
  }
}

export default TestPage;