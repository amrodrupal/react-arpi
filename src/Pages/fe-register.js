import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import axios from "axios";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      isLoaded: '',
      items: [],
      error: null
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  matchPasswords() {
    const password = document.getElementById('password').value;
    const passwordRpt = document.getElementById('password-rpt').value;
    console.log(password===passwordRpt ? 'Match' : 'Not match');
    return (password===passwordRpt);
  }

  register(username, email, password) {

    const config = {
      query: `
mutation {
  UserRegister(
    input: {
      name: "${username}",
      mail: "${email}",
      password: "${password}"
    }
  ) {
    violations {
      path
      message
    }
  }
}`};
    axios.post(
      "http://fw-backend.local/graphql",
        config,
    ).then((response) => {
      console.log(response);
      this.setState({
        isLoaded: false,
        items: response
      });
    }).catch((error) => {
      console.log(error);
      this.setState({
        isLoaded: true,
        error
      });
    });
  }

  onSubmit(e) {
    e.preventDefault();

    this.setState({isLoaded: true});
    if (this.matchPasswords()) {
      const username = document.getElementById('username').value;
      const email = document.getElementById('email').value;
      const password = document.getElementById('password').value;

      if (username && email && password) {
        this.register(username, email, password);
      }
    }

  }

  render() {

    const { isLoaded } = this.state;
    return (
      <Container className={'register-container'}>
        <Form onSubmit={this.onSubmit} className={'register-form'}>
          <Form.Group controlId={'username'}>
            <Form.Label>Username:</Form.Label>
            <Form.Control type={'text'} required/>
          </Form.Group>
          <Form.Group controlId={'email'}>
            <Form.Label>Email:</Form.Label>
            <Form.Control type={'email'} required/>
          </Form.Group>

          <Form.Group controlId={'password'}>
            <Form.Label>Password:</Form.Label>
            <Form.Control type={'password'} required/>
          </Form.Group>

          <Form.Group controlId={'password-rpt'}>
            <Form.Label>Repeat password:</Form.Label>
            <Form.Control type={'password'} required/>
          </Form.Group>

          <Button variant={'primary'} type={'submit'} disabled={isLoaded}>Register</Button>
        </Form>
      </Container>
    )
  }
}

export default Register;
