import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Homepage from "../Pages/fe-homepage";
import Login from "../Pages/fe-login";
import Register from "../Pages/fe-register";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import TestRequest from "../Pages/fe-testrequest";

export default function Routes() {

  return (
    <Router>
      <Navbar bg={'dark'} variant={'dark'}>
        <Container className={'navbar-container'}>
          <Navbar.Brand href={'/'}>Home</Navbar.Brand>
          <Nav className={'main-nav'}>
            <Nav.Link href={'/login'}>Login</Nav.Link>
            <Nav.Link href={'/register'}>Register</Nav.Link>
            <Nav.Link href={'/test'}>Test Request</Nav.Link>
          </Nav>

        </Container>
      </Navbar>


      <Switch>
        <Route exact path={"/"} component={Homepage}/>
        <Route path={"/login"} component={Login}/>
        <Route path={"/register"} component={Register}/>
        <Route path={'/test'} component={TestRequest}/>
      </Switch>
    </Router>
  );
}
