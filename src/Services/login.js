import axios from 'axios';

export const Service = {
  login,
};
const qs = require('querystring');

async function login(username, password) {


  try {
    const config = {
      client_id: 'd994d914-749a-47bc-90d9-2e24c32b22f8',
      client_secret: 'secret',
      grant_type: 'password',
      username,
      password,
    };
    const response = await axios.post(
      'http://fw-backend.local/oauth/token',
      qs.stringify(config)
        .then((result) => {
            this.setState({
              isLoaded: true,
              items: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          })
    );
    console.log('Response:' + response);
  } catch(e) {
    console.log('Request failed:' + e);
  }
}
